module.exports = {
    basePath: "/ascii-art-webcam",
    assetPrefix: "/ascii-art-webcam",
    publicRuntimeConfig: {
        // Will be available on client
        prefix: "/ascii-art-webcam",
    },
};
