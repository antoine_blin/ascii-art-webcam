import { FC, useState } from "react";

import { IconButton, makeStyles, Theme } from "@material-ui/core";
import { Pause, PlayArrow } from "@material-ui/icons";
import dynamic from "next/dynamic";

// to avoid a window access on the SSR
const AsciiCam = dynamic(() => import("../components/AsciiCam"), {
  ssr: false,
});

const useStyles = makeStyles<Theme>(() => ({
  playPauseIconButton: {
    fontSize: "10rem",
  },
}));

const Home: FC = function () {
  const classes = useStyles();
  const [play, setPlay] = useState<boolean>(true);

  const handleOnAsciiCamError = () => {
    console.log("Unable to use the webcam");
  };

  const handlePlayButtonClick = () => {
    setPlay(!play);
  };

  return (
    <div>
      <IconButton
        style={{ position: "absolute" }}
        onClick={handlePlayButtonClick}
      >
        {play ? (
          <Pause className={classes.playPauseIconButton} />
        ) : (
          <PlayArrow className={classes.playPauseIconButton} />
        )}
      </IconButton>
      <AsciiCam onError={handleOnAsciiCamError} play={play} />
    </div>
  );
};

export default Home;
