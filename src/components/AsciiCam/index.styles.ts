import { makeStyles, Theme } from "@material-ui/core";

export const useStyles = makeStyles<Theme, { fontSize: number }>(() => ({
  asciiContainer: ({ fontSize }) => ({
    margin: 0,
    fontSize: `${fontSize}px`,
  }),
}));
