import React, { useEffect, useRef, useState } from "react";

import { canvasToAscii } from "../../libs/ascii-utils";
import { useStyles } from "./index.styles";

export type AsciiCamProps = {
  play?: boolean;
  mirror?: boolean;
  fps?: number;
  contrast?: number;
  fontSize?: number;
  onError?: () => void;
};

export type CharactersNumbers = {
  numberOfCharactersByLine: number;
  numberOfCharactersByColumn: number;
};

declare const navigator: any;
declare const window: any;

/**
 * Functionnal component converting the video stream from webcam to an ascii art string
 *
 * @param play if false then the stream is stopped
 * @param mirror set to true to invert camera result
 * @param fps number of frame per seconds
 * @param contrast constrast number
 * @param fontSize size of characters
 * @param onError callback method if the webcam is not available
 */
const AsciiCam = function ({
  play = true,
  fps = 20,
  mirror = true,
  contrast = 128,
  fontSize = 15,
  onError,
}: AsciiCamProps) {
  const classes = useStyles({ fontSize });
  const [asciiCharacters, setAsciiCharacters] = useState<string>("");
  const videoContainerRef = useRef(null);
  const canvasContainerRef = useRef(null);
  let context;
  let renderTimer;

  const getCharactersNumbersForForWindow = (): CharactersNumbers => {
    return {
      numberOfCharactersByLine: window.innerWidth / (fontSize / 2),
      numberOfCharactersByColumn: window.innerHeight / fontSize,
    };
  };

  const [numberOfCharacters, setNumberOfCharacters] =
    useState<CharactersNumbers>(getCharactersNumbersForForWindow());

  // initialize the canvas
  useEffect(() => {
    videoContainerRef.current.setAttribute("playsinline", "true");
    videoContainerRef.current.setAttribute("webkit-playsinline", "true");

    navigator.getUserMedia =
      navigator.getUserMedia ||
      navigator.webkitGetUserMedia ||
      navigator.mozGetUserMedia ||
      navigator.msGetUserMedia;
    window.URL =
      window.URL || window.webkitURL || window.mozURL || window.msURL;
    if (navigator.getUserMedia) {
      navigator.getUserMedia(
        {
          video: true,
          audio: false,
        },
        (stream) => {
          if (videoContainerRef.current.mozSrcObject !== undefined) {
            videoContainerRef.current.mozSrcObject = stream;
          } else {
            videoContainerRef.current.srcObject = stream;
          }
          initCanvas();
          play && startCapture();
        },
        handleOnError
      );
    } else {
      handleOnError();
    }
  }, [videoContainerRef]);

  // update characters number on window resizing
  useEffect(() => {
    const resizeListener = () => {
      setNumberOfCharacters(getCharactersNumbersForForWindow());
    };
    window.addEventListener("resize", resizeListener);
    return () => {
      window.removeEventListener("resize", resizeListener);
    };
  }, []);

  // react on the play state
  useEffect(() => {
    if (!play) {
      onPauseCapture();
    } else {
      startCapture();
    }
  }, [play]);

  const initCanvas = () => {
    canvasContainerRef.current.setAttribute(
      "width",
      numberOfCharacters.numberOfCharactersByLine.toString()
    );
    canvasContainerRef.current.setAttribute(
      "height",
      numberOfCharacters.numberOfCharactersByColumn.toString()
    );
    context = canvasContainerRef.current.getContext("2d");
    if (mirror) {
      context.translate(canvasContainerRef.current.width, 0);
      context.scale(-1, 1);
    }
  };

  const handleOnError = () => {
    onError && onError();
  };

  const startCapture = () => {
    videoContainerRef.current.play();
    renderTimer = setInterval(function () {
      try {
        context.drawImage(
          videoContainerRef.current,
          0,
          0,
          videoContainerRef.current.width,
          videoContainerRef.current.height
        );
        const newAsciiCharacters = canvasToAscii(
          canvasContainerRef.current,
          contrast
        );
        setAsciiCharacters(newAsciiCharacters);
      } catch (e) {
        // TODO
      }
    }, Math.round(1000 / fps));
  };

  const onPauseCapture = () => {
    if (renderTimer) {
      clearInterval(renderTimer);
    }
    videoContainerRef.current.pause();
  };

  return (
    <div>
      <video
        style={{ display: "none" }}
        ref={videoContainerRef}
        width={numberOfCharacters.numberOfCharactersByLine}
        height={numberOfCharacters.numberOfCharactersByColumn}
      />
      <canvas
        style={{ display: "none" }}
        ref={canvasContainerRef}
        width={numberOfCharacters.numberOfCharactersByLine}
        height={numberOfCharacters.numberOfCharactersByColumn}
      />
      <pre className={classes.asciiContainer}> {asciiCharacters}</pre>
    </div>
  );
};

export default AsciiCam;
